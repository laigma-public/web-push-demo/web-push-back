<img src="./src/assets/lp-dark.png" alt="Ligma Prods" height="60"/>
<img src="./src/assets/nodejs-logo.png" alt="NodeJS" height="60"/>
<img src="./src/assets/ts.png" alt="TypeScript" height="60"/>

# Web Push Demo (NodeJS + TypeScript API)

## Requisitos
- node 16.x o superior
- npm

## Instrucciones

### 1- Instalar dependencias
```
npm install
```

### 2- Crear .env en carpeta raiz
```
API_PROTOCOL=http
API_HOST=localhost
API_PORT=8001
```
   
### 4- Compilar y ejecutar
#### Servidor caliente
```
npm run dev
```
#### Compila y ejecuta
```
npm run prod
```

----

## Enlaces de interés
- https://expressjs.com/es/
- https://www.typescriptlang.org/
- https://github.com/web-push-libs/web-push

____

## Pasos para configurar web-push

1. Instalar libreria
```
npm install web-push
npm i @types/web-push -D
```

2. Generar par de claves VAPID
```
npx web-push generate-vapid-keys [--json]
```

3. Añadir claves en .env
```
API_PROTOCOL=http
API_HOST=localhost
API_PORT=8001

PUBLIC_VAPID_KEY=<tu_clave_publica>
PRIVATE_VAPID_KEY=<tu_clave_privada>
```

4. Generar servicio de webPush

5. Generar rutas para suscripcion y envio de notificaciones
