import dotenv from 'dotenv'
import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import helmet from 'helmet'
import bodyParser from 'body-parser'
import http from 'http'
import https from 'https'
import fs from 'fs'

import routes from './routes'

const apiProtocol = process.env.API_PROTOCOL || 'http';
const apiHost = process.env.API_HOST || 'localhost';
const apiPort = process.env.API_PORT || 8001

dotenv.config()
const app = express()

app.use(cors())
app.use(morgan('dev'))
app.use(morgan(':date[iso] \n'))
app.use(helmet.contentSecurityPolicy({
  directives: {
    'script-src': ['\'unsafe-inline\'', '*', '\'unsafe-eval\''],
    'style-src': ['\'unsafe-inline\'', '*'],
    'connect-src': ['\'unsafe-inline\'', '*'],
  },
}))
app.disable('x-powered-by')
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(routes)

if (apiProtocol === 'https') { // Inicia el servidor API REST (https)
  https.createServer({
    key: fs.readFileSync('/etc/ssl/private/laigma.ddns.net.key'),
    cert: fs.readFileSync('/etc/ssl/certs/laigma.ddns.net.pem-chain')
  }, app).listen(apiPort, function () {
    console.log(`\nServidor está disponible en \x1b[32m https://${apiHost}:${apiPort} \x1b[0m \n`);
  });

} else {  // Inicia el servidor API REST (http)
  http.createServer(app).listen(apiPort, function () {
    console.log(`\nServidor está disponible en \x1b[32m http://${apiHost}:${apiPort} \x1b[0m \n`);
  });
}
