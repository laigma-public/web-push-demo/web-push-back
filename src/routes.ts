import { Router, response } from 'express';
import webPushService from './services/webPushService';
import cryptoService from './services/cryptoService';
import { Subscription, SubscriptionObject } from './types';

const router = Router();

// Base de datos en memoria
let pushSubscriptions = [] as any[];

router.get('/', (req, res) => {
  res.status(200).json({ status: "S", log: "API REST funcionando correctamente" });
});

router.post('/webPush/subscribe', (req, res) => {
  const data = req.body;
  console.log(`\nDatos recibidos: ${JSON.stringify(data)}`);

  const encryptedSubscription = data.body;
  const decryptedSubscription = JSON.parse(cryptoService.decrypt(encryptedSubscription)) as Subscription;

  // Guardamos subscripcion
  pushSubscriptions.push(decryptedSubscription);

  console.log(`\nLista de suscripciones: ${JSON.stringify(pushSubscriptions)}`);
  res.status(201).json({ status: "S", log: "Registrada correctamente" });
});

router.post('/webPush/unsubscribe', (req, res) => {
  const data = req.body;
  console.log(`\nDatos recibidos: ${JSON.stringify(data)}`);

  const encryptedSubscription = data.body;
  const decryptedSubscription = JSON.parse(cryptoService.decrypt(encryptedSubscription)) as SubscriptionObject;

  // Extraemos endpoint para eliminar la subscripcion
  const endpoint = decryptedSubscription.endpoint;

  // Buscamos la subscripcion a eliminar por el endpoint del objeto subscriptionObject
  const index = pushSubscriptions.findIndex((subscription) => subscription.subscriptionObject.endpoint === endpoint);
  pushSubscriptions.splice(index, 1);

  console.log(`\nLista de suscripciones: ${JSON.stringify(pushSubscriptions)}`);
  res.status(201).json({ status: "S", log: "Eliminada correctamente" });
});

router.post('/webPush/sendNotification', async (req, res) => {
  const { title, body, application } = req.body;
  if (!title || !body || !application) {
    res.json({ status: "E", log: "Faltan parámetros", data: [] });
    return;
  }

  // Filtramos las suscripciones por la aplicacion
  const appSubscriptions = pushSubscriptions.filter((subscription: Subscription) => subscription.application === application);

  const promises = appSubscriptions.map((subscription: Subscription) =>
    webPushService.sendNotification(subscription, JSON.stringify({ title, body }))
      .then(response => response)
      .catch(error => console.log(error.message || error))
  );
  const result = await Promise.allSettled(promises);

  // Filtramos y eliminamos las suscripciones que ya no son válidas
  result.forEach(responseData => {
    if (responseData.status === "fulfilled") return;
    if (responseData.status === "rejected") {
      const errorData = JSON.parse(responseData.reason.data);
      if (errorData.name === "WebPushError" && errorData.statusCode === 410) {
        // Elimina de la lista de suscripciones la suscripcion que ya no es valida
        const index = pushSubscriptions.findIndex((subscription) => subscription.ID === responseData.reason.subscription.ID);
        pushSubscriptions.splice(index, 1);
      }
    }
  });

  res.json({ status: "S", log: "Notificacion enviada", data: result });
});

export default router;
