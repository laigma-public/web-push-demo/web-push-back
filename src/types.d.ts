export type Subscription = {
  ID: string;
  Fecha_suscripcion: string;
  application: string;
  user: string;
  subscriptionObject: SubscriptionObject;
}

export type SubscriptionObject = {
  endpoint: string;
  expirationTime: string;
  keys: {
    p256dh: string;
    auth: string;
  }
}

export type SendNotificationResponse = {
  status: string;
  subscription: Subscription;
  data: string;
}

export type SubscriptionResponse = {
  ID: string;
  application: string;
  status: string;
}
