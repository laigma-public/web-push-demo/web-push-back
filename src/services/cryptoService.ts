import dotenv from "dotenv";
import CryptoJS from "crypto-js";

dotenv.config();

const secretKey = process.env.AES_KEY as string;

export default {
  encrypt(src: string) {
    return CryptoJS.AES.encrypt(src, secretKey).toString()
  },

  decrypt(src: string) {
    const bytes = CryptoJS.AES.decrypt(src, secretKey)
    const originalText = bytes.toString(CryptoJS.enc.Utf8)
    return originalText
  }
}
