import dotenv from 'dotenv'
import webpush from 'web-push';
import { Subscription, SendNotificationResponse } from '../types';

dotenv.config()

const mailto = process.env.MAILTO as string;
const publicVapidKey = process.env.VAPID_PUBLIC_KEY as string;
const privateVapidKey = process.env.VAPID_PRIVATE_KEY as string;

webpush.setVapidDetails(mailto, publicVapidKey, privateVapidKey);

export default {
  async sendNotification(subscription: Subscription, payload: string) {
    return webpush.sendNotification(subscription.subscriptionObject, payload)
      .then(response => {
        console.log(`\nRespuesta de la notificación:\n${response}`);
        return ({ status: "OK", subscription, data: JSON.stringify(response) }) as SendNotificationResponse;
      })
      .catch(error => console.log(`\nError en la notificación:\n${error}`));
  }
}
