# Usar una imagen de Node.js
FROM node:18.12.1
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 8002
CMD ["npm", "run", "start"]
